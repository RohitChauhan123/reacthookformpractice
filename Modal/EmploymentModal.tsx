import React from "react";
import Dropdown from "../Components/selectBox";
import { Container } from "../styles/Modal/EmploymentModal";
import { useForm } from "react-hook-form";

export const EmploymentModal = (props: any) => {
  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm();

  const onSubmit = (data: any) => console.log(data);

  if (!props.show) {
    return null;
  }
  return (
    <Container>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="modal">
          <div className="modal-contect">
            <div className="modal-header">
              <h1 className="modal-title">Add Employment</h1>
            </div>
            <div className="modal-body">
              <p>Is this your current employment?</p>
              <input
                type="radio"
                id="age1"
                name="current employment"
                value="30"
              />
              <span>Yes</span>
              <input
                type="radio"
                id="age2"
                name="current employment"
                value="60"
              />
              <span>No</span>
              <p>Employment Type</p>
              <input type="radio" id="age1" name="employmentRadio" value="30" />
              <span>Full Time </span>
              <input type="radio" id="age2" name="employmentRadio" value="60" />
              <span>Internship</span>
              <div>
                <h5>
                  Total Experience<sup>*</sup>
                </h5>
                <div
                  style={{
                    display: "flex",
                    gap: 5,
                    justifyContent: "space-around",
                    width: " 100%",
                  }}
                >
                  <Dropdown control={control} />
                  <Dropdown control={control} />
                </div>
                <div>
                  <h5>
                    Previous Company Name{" "}
                    <sup style={{ color: "red", width: "5px" }}>*</sup>
                  </h5>
                  <input
                    type="text"
                    style={{
                      //   border: "1px solid red",
                      outline: "none",
                      width: "80%",
                      height: "40px",
                    }}
                    placeholder="type your organization"
                  />
                </div>
                <div>
                  <h5>
                    Previous Designation
                    <sup style={{ color: "red", width: "5px" }}>*</sup>
                  </h5>
                  <input
                    type="text"
                    style={{
                      //   border: "1px solid red",
                      outline: "none",
                      width: "80%",
                      height: "40px",
                    }}
                    placeholder="type your organization"
                  />
                </div>

                <div>
                  <h5>
                    Joining Data
                    <sup style={{ color: "red", width: "5px" }}>*</sup>
                  </h5>
                  <div
                    style={{
                      display: "flex",
                      gap: 5,
                      justifyContent: "space-around",
                      width: " 100%",
                    }}
                  >
                    <Dropdown control={control} />
                    <Dropdown control={control} />
                  </div>
                </div>

                <div>
                  <p>Current Annual Salary</p>
                  <input type="radio" id="age1" name="salary" value="30" />
                  <span>Indian Rupees</span>
                  <input type="radio" id="age2" name="salary" value="60" />
                  <span>US Dollar</span>
                </div>
                <div
                  style={{
                    display: "flex",
                    gap: 5,
                    justifyContent: "space-around",
                    width: " 100%",
                  }}
                >
                  <Dropdown control={control} />
                  <Dropdown control={control} />
                </div>

                <div>
                  <h5>
                    Skills Used
                    <sup style={{ color: "red", width: "5px" }}>*</sup>
                  </h5>
                  <input
                    type="text"
                    style={{
                      //   border: "1px solid red",
                      outline: "none",
                      width: "80%",
                      height: "40px",
                    }}
                    placeholder="type your organization"
                  />
                </div>
              </div>
            </div>
            <div className="modal-footer">Close</div>
          </div>
        </div>
      </form>
    </Container>
  );
};
