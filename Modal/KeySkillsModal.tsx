import React from "react";
import { Container } from "../styles/Modal/KeySkillsModal";

export const KeySkillsModal = () => {
  return (
    <Container>
      <div className="modal">
        <div className="modal-contect">
          <div className="modal-header">
            <h1 className="modal-title">modal Title</h1>
          </div>
          <div className="modal-body">
            <form action="">
              <p style={{ fontSize: "10px" }}>
                Tell recruiters what you know or what you are known for e.g.
                Direct Marketing, Oracle, Java etc. We will send you job
                recommendations based on these skills. Each skill is separated
                by a comma.
              </p>
              Skills:
              <select></select>
            </form>
          </div>
          <div className="modal-footer">Close</div>
        </div>
      </div>
    </Container>
  );
};
