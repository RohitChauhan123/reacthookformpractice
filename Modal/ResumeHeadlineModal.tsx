import React from "react";
import { Container } from "../styles/Modal/ResumeHeadlineModal";

export const ResumeHeadlineModal = (props: any) => {
  if (!props.show) {
    return null;
  }
  return (
    <>
      <Container>
        <div className="modal">
          <div className="modal-contect">
            <div className="modal-body">
              <form action="">
                <div className="modal-header">
                  <h1 className="modal-title">Resume Headline</h1>
                  <p style={{ fontSize: "10px" }}>
                    It is the first thing recruiters notice in your profile.
                    Write concisely what makes you unique and right person for
                    the job you are looking for.
                  </p>

                  <textarea name="" id="" cols="50" rows="10"></textarea>
                </div>
                <button className="modal-footer">Close</button>
                <button className="modal-footer">Save</button>

                {/* <div className="modal-footer">Close</div>
              <div className="modal-footer">save</div> */}
              </form>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};
