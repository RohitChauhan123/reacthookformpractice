import { useState, createContext } from "react";

interface Request {
  resume: any;
  resumeHeadline: string;
  skills: [];
  currentlyWorking: boolean;
  employmentType: string;
  totalExperience: string;
  currentCompanyName: string;
  currentDesignation: string;
  joiningDate: string;
  currentAnnualSalary: string;
  SkillsUsed: [];
  jobProfile: string;
  noticePeriod: string;
  education: [{}];
  Course: string;
  Specialization: string;
  UniversityInstitute: string;
  courseType: string;
  passingOutYear: string;
  gradingSystem: string;
  projectTitle: string;
  projectStatus: string;
  projectWorkedfrom: string;
  detailsOfProject: string;
}

export const defaultRequest: Request = {
  resume: null,
  resumeHeadline: "",
  skills: [],
  currentlyWorking: false,
  employmentType: "",
  totalExperience: "",
  currentCompanyName: "",
  currentDesignation: "",
  joiningDate: "",
  currentAnnualSalary: "",
  SkillsUsed: [],
  jobProfile: "",
  noticePeriod: "",
  education: [{}],
  Course: "",
  Specialization: "",
  UniversityInstitute: "",
  courseType: "",
  passingOutYear: "",
  gradingSystem: "",
  projectTitle: "",
  projectStatus: "",
  projectWorkedfrom: "",
  detailsOfProject: "",
};

export const ArtistContext = createContext<any>(defaultRequest);

export const ArtistProvider = ({ children }: any) => {
  const [data, setData] = useState<Request>(defaultRequest);
  const value = { data, setData };

  return (
    <ArtistContext.Provider value={value}>{children}</ArtistContext.Provider>
  );
};
