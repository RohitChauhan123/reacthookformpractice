// import { useContext } from "react";
// import Select from "react-select";
// import DownArrowImg from "assets/images/arrow-down-black.png";
// import DownArrowWhiteImg from "assets/images/arrow-down-white.png";
// import Image from "next/image";
// import { ModeContext } from "context/mode";
// import { Container, DownArrow } from "styles/components/Dropdown";
// import { Controller } from "react-hook-form";
import { Controller } from "react-hook-form";
import Select from "react-select";

export interface DropDownProps {
  multiSelect?: boolean;
  placeholder?: string;
  name?: string;
  data?: any;
  isDisabled?: boolean;
  value?: any;
  defaultValue?: any;
  isSearchable?: boolean;
  className?: string;
  onChange?: any;
  control?: any;
}

const Dropdown = ({
  multiSelect,
  placeholder,
  data,
  isDisabled,
  defaultValue,
  isSearchable,
  className,
  name,
  control,
}: DropDownProps) => {
  return (
    <div className={className}>
      <Controller
        control={control}
        render={({ field: { onChange, value } }) => (
          <Select
            isMulti={multiSelect}
            placeholder={placeholder}
            options={data}
            classNamePrefix="react-select"
            isDisabled={isDisabled}
            value={value}
            defaultValue={defaultValue}
            isSearchable={isSearchable}
            onChange={onChange}
            name={name}
          />
        )}
        name={name || ""}
      />
    </div>
  );
};

export default Dropdown;
