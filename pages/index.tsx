import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import { Profile } from "./Profile/index";

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <Profile />
    </div>
  );
};

export default Home;
