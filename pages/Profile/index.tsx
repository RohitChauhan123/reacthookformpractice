import React, { useState } from "react";
import { EmploymentModal } from "../../Modal/EmploymentModal";
import { ResumeHeadlineModal } from "../../Modal/ResumeHeadlineModal";
import Dropdown from "../../Components/selectBox";
import "react-responsive-modal/styles.css";
import { Modal } from "react-responsive-modal";
import { useForm } from "react-hook-form";
import {
  // Container,
  // Education,
  // Employment,
  // KeySlills,
  // Project,
  // ResumeHeadline,
  // ResumeWrapper,
  MainWrapper,
  FormContainer,
  Input,
  DataWrapper,
  HeadingWrapper,
  Button,
} from "../../styles/Profile";

const years = [
  { value: "0", label: "0" },
  { value: "1", label: "1" },
  { value: "2", label: "2" },
  { value: "3", label: "3" },
  { value: "4", label: "4" },
  { value: "5", label: "5" },
  { value: "6", label: "6" },
];

const Lac = [
  { value: "0", label: "0" },
  { value: "1Lac", label: "1Lac" },
  { value: "2Lac", label: "2Lac" },
  { value: "3Lac", label: "3Lac" },
  { value: "4Lac", label: "4Lac" },
  { value: "4Lac", label: "4Lac" },
  { value: "5Lac", label: "5Lac" },
  { value: "6Lac", label: "6Lac" },
  { value: "7Lac", label: "7Lac" },
  { value: "8Lac", label: "8Lac" },
  { value: "9Lac", label: "9Lac" },
  { value: "10+Lac", label: "10lacs" },
  { value: "15Lac", label: "11Lac" },
  { value: "16Lac", label: "11Lac" },
];

const Thousand = [
  { value: "0", label: "0" },
  { value: "5L Thousandac", label: "5 Thousand" },
  { value: "10 Thousand", label: "10 Thousand" },
  { value: "15 Thousand", label: "15 Thousand" },
  { value: "20 Thousand", label: "20Thousand" },
  { value: "25 Thousand", label: "25Thousand" },
  { value: "30 Thousand", label: "30 Thousand" },
  { value: "40 Thousand", label: "40 Thousand" },
  { value: "50 Thousand", label: "50Thousand" },
  { value: "60 Thousand", label: "60Thousand" },
];

const months = [
  { value: "0", label: "0" },
  { value: "1", label: "1" },
  { value: "2", label: "2" },
  { value: "3", label: "3" },
  { value: "4", label: "4" },
  { value: "4", label: "4" },
  { value: "5", label: "5" },
  { value: "6", label: "6" },
  { value: "7", label: "7" },
  { value: "8", label: "8" },
  { value: "9", label: "9" },
  { value: "10", label: "10" },
  { value: "11", label: "11" },
];

export const Profile = () => {
  const [open, setOpen] = useState(false);
  const [openSecond, setOpenSecond] = useState(false);
  const [openThird, setOpenThird] = useState(false);
  const [openFourth, setOpenFourth] = useState(false);

  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm({
    defaultValues: {
      resumeHeadline: null,
      keySkills: "",
      currentEmployment: "",
      employmentType: "",
      totalExperience: {
        years: "",
        months: "",
      },
    },
  });

  const onSubmit = (data: any) => console.log(data);

  const onCloseModal = () => setOpen(false);
  const onCloseSecond = () => setOpenSecond(false);
  const onCloseThird = () => setOpenThird(false);
  const onCloseFourth = () => setOpenFourth(false);

  return (
    <MainWrapper>
      <FormContainer>
        <div className="resumeWrapper">
          <h6>Resume</h6>
          <p style={{ fontSize: "8px" }}>
            Resume is the most important document recruiters look for.
            Recruiters generally do not look at profiles without resumes.
          </p>
          <div className="btn-wraper">
            <button className="btn">UPLOAD RESUME</button>
          </div>
        </div>

        <div className="resumeHadlingWrapper">
          <DataWrapper>
            <HeadingWrapper>
              <h5> Resume Headline</h5>
              <p style={{ fontSize: "10px" }}>add</p>
            </HeadingWrapper>
            <button
              className="button"
              onClick={() => {
                setOpen(true);
              }}
            >
              ADD RESUME HEADLINE
            </button>
          </DataWrapper>
          <p style={{ fontSize: "8px", color: "gray" }}>
            It is the first thing recruiters notice in your profile. Write
            concisely what makes you unique and right person for the job you are
            looking for.
          </p>
          <Modal
            open={open}
            onClose={onCloseModal}
            center
            classNames={{
              overlay: "customOverlay",
              modal: "customModal",
            }}
          >
            <form onSubmit={handleSubmit(onSubmit)}>
              <div>
                <h1>Resume Headline</h1>
                <p style={{ fontSize: "8px" }}>
                  It is the first thing recruiters notice in your profile.
                  <p>
                    Write concisely what makes you unique and right person for
                    the job you are looking for.
                  </p>
                </p>

                <textarea
                  name="resumeHeadline"
                  id=""
                  cols="50"
                  rows="10"
                ></textarea>
              </div>
              <button type="button">Close</button>
              <button>Save</button>
            </form>
          </Modal>
        </div>

        <div className="keySkiilsWrapper">
          <DataWrapper>
            <HeadingWrapper>
              <h6>Key Skills</h6>
              <p style={{ fontSize: "10px" }}>add</p>
            </HeadingWrapper>
            <button
              className="button"
              onClick={() => {
                setOpenSecond(true);
              }}
            >
              ADD KEY SKILLS
            </button>
          </DataWrapper>

          <p style={{ fontSize: "8px", color: "gray" }}>
            Mention your employment details including your current and previous
            company work experience.
          </p>

          <Modal
            open={openSecond}
            onClose={onCloseSecond}
            center
            classNames={{
              overlay: "customOverlay",
              modal: "customModal",
            }}
          >
            <form onSubmit={handleSubmit(onSubmit)}>
              <div>
                <h1>Key Skills</h1>
                <p style={{ fontSize: "8px", color: "gray" }}>
                  Tell recruiters what you know or what you are known for e.g.
                  Direct Marketing, Oracle, Java etc. We will send you job
                  recommendations based on these skills. Each skill is separated
                  by a comma.
                </p>
                <input
                  className="text-input"
                  type="text"
                  {...register("keySkills")}
                  // style={{
                  //   outline: "none",
                  //   width: "80%",
                  //   height: "40px",
                  // }}
                />
              </div>
              <br />
              <button type="button">Close</button>
              <button>Save</button>
            </form>
          </Modal>
        </div>

        <div className="EmploymentWraper">
          <button
            className="button"
            onClick={() => {
              setOpenThird(true);
            }}
          >
            ADD EMPLOYMENT
          </button>
          <h6>Employment</h6>
          <p style={{ fontSize: "8px", color: "gray" }}>
            Mention your employment details including your current and previous
            company work experience.
          </p>
          <div>
            <Modal
              open={openThird}
              onClose={onCloseThird}
              center
              classNames={{
                overlay: "customOverlay",
                modal: "customModal",
              }}
            >
              <form onSubmit={handleSubmit(onSubmit)}>
                <div>
                  <h1>Add Employment</h1>
                </div>
                <div>
                  <p>Is this your current employment?</p>
                  <input
                    type="radio"
                    id="age1"
                    name="currentEmployment"
                    {...register("currentEmployment")}
                    value="Yes"
                  />
                  <span>Yes</span>
                  <input
                    type="radio"
                    id="age2"
                    name="currentEmployment"
                    {...register("currentEmployment")}
                    value="No"
                  />
                  <span>No</span>

                  <p>Employment Type</p>
                  <input
                    type="radio"
                    id="age1"
                    name="employmentType"
                    {...register("employmentType")}
                    value="Full Time"
                  />
                  <span>Full Time </span>
                  <input
                    type="radio"
                    id="age2"
                    name="employmentType"
                    {...register("employmentType")}
                    value="Part Time"
                  />
                  <span>Internship</span>
                  <div>
                    <h5>
                      Total Experience<sup>*</sup>
                    </h5>
                    <div
                      style={{
                        display: "flex",
                        gap: 5,
                        justifyContent: "space-around",
                        width: " 100%",
                      }}
                    >
                      <Dropdown
                        control={control}
                        data={years}
                        placeholder="Select year"
                        {...register(`totalExperience.years`)}
                      />
                      <Dropdown
                        control={control}
                        data={months}
                        placeholder="Select   month"
                        {...register(`totalExperience.months`)}
                      />
                    </div>
                    <div>
                      <h5>
                        Previous Company Name{" "}
                        <sup style={{ color: "red", width: "5px" }}>*</sup>
                      </h5>
                      <Input
                        type="text"
                        className="text-input"
                        placeholder="type your organization"
                      />
                    </div>
                    <div>
                      <h5>
                        Previous Designation
                        <sup style={{ color: "red", width: "5px" }}>*</sup>
                      </h5>
                      <input
                        type="text"
                        className="text-input"
                        // style={{
                        //   outline: "none",
                        //   width: "80%",
                        //   height: "40px",
                        // }}
                        placeholder="type your organization"
                      />
                    </div>

                    <div>
                      <h5>
                        Joining Data
                        <sup style={{ color: "red", width: "5px" }}>*</sup>
                      </h5>
                      <div
                        style={{
                          display: "flex",
                          gap: 5,
                          justifyContent: "space-around",
                          width: " 100%",
                        }}
                      >
                        <Dropdown
                          control={control}
                          data={years}
                          placeholder="Select year"
                        />
                        <Dropdown
                          control={control}
                          data={months}
                          placeholder="Select   month"
                        />
                      </div>
                    </div>

                    <div>
                      <p>Current Annual Salary</p>
                      <input
                        type="radio"
                        id="age1"
                        name="salary"
                        value="Indian Rupees"
                      />
                      <span>Indian Rupees</span>
                      <input
                        type="radio"
                        id="age2"
                        name="salary"
                        value="US Dollar"
                      />
                      <span>US Dollar</span>
                    </div>
                    <div
                      style={{
                        display: "flex",
                        gap: 5,
                        justifyContent: "space-around",
                        width: " 100%",
                      }}
                    >
                      <Dropdown
                        control={control}
                        data={Lac}
                        placeholder="Lacs"
                      />
                      <Dropdown
                        control={control}
                        data={Thousand}
                        placeholder="Thousand"
                      />
                    </div>

                    <div>
                      <h5>
                        Skills Used
                        <sup style={{ color: "red", width: "5px" }}>*</sup>
                      </h5>
                      <div className="text-input">
                        <input
                          type="text"
                          // style={{
                          //   outline: "none",
                          //   width: "80%",
                          //   height: "40px",
                          // }}
                          placeholder="type your organization"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <br />
                <button type="button">Close</button>
                <button>Save</button>
              </form>
            </Modal>
          </div>
        </div>

        <div className="EducationWraper ">
          <button
            className="button"
            onClick={() => {
              setOpenFourth(true);
            }}
          >
            ADD EDUCATION
          </button>
          <h6>Education</h6>
          <p style={{ fontSize: "8px", color: "gray" }}>
            Please mention your education details. You can add details about
            your school, college and degree. This will increase your profile
            strength.
          </p>

          <Modal
            open={openFourth}
            onClose={onCloseFourth}
            center
            classNames={{
              overlay: "customOverlay",
              modal: "customModal",
            }}
          >
            <form onSubmit={handleSubmit(onSubmit)}>
              <div>
                <h1>Add Employment</h1>
              </div>
              <div>
                <div>
                  <h5>
                    Education<sup>*</sup>
                  </h5>
                  <div
                    style={{
                      display: "flex",
                      gap: 5,
                      justifyContent: "space-around",
                      width: " 100%",
                    }}
                  ></div>
                  <div>
                    <Dropdown control={control} />
                  </div>

                  <div>
                    <h5>
                      Course<sup>*</sup>
                    </h5>
                    <div
                      style={{
                        display: "flex",
                        gap: 5,
                        justifyContent: "space-around",
                        width: " 100%",
                      }}
                    ></div>
                    <div>
                      <Dropdown control={control} />
                    </div>
                  </div>

                  <div>
                    <h5>
                      Specialization<sup>*</sup>
                    </h5>
                    <div
                      style={{
                        display: "flex",
                        gap: 5,
                        justifyContent: "space-around",
                        width: " 100%",
                      }}
                    ></div>
                    <div>
                      <Dropdown control={control} />
                    </div>
                  </div>

                  <div>
                    <h5>Course Type</h5>
                    <input type="radio" id="age1" name="Course" value="30" />
                    <span>Full Time </span>
                    <input type="radio" id="age2" name="Course" value="60" />
                    <span>Part Time</span>
                  </div>
                </div>
              </div>
              <div>
                <h5>
                  PassOut Year<sup>*</sup>
                </h5>
                <div
                  style={{
                    display: "flex",
                    gap: 5,
                    justifyContent: "space-around",
                    width: " 100%",
                  }}
                >
                  <Dropdown control={control} />
                </div>
              </div>
              <br />
              <button type="button">Close</button>
              <button>Save</button>
            </form>
          </Modal>
        </div>

        <div className="ProjectWraper">
          <button className="button">ADD PROJECT</button>
          <h6>Project</h6>
          <p style={{ fontSize: "8px", color: "gray" }}>
            Add details about projects you have done in college, internship or
            at work.
          </p>
        </div>
      </FormContainer>
    </MainWrapper>
  );
};
