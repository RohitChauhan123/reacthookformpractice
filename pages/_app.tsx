import "../styles/globals.css";
import type { AppProps } from "next/app";
import { ArtistProvider } from "../context/ProfileDetails";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ArtistProvider initialData={pageProps?.initialData}>
      <Component {...pageProps} />
    </ArtistProvider>
  );
}

export default MyApp;
