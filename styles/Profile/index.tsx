import styled from "styled-components";

export const MainWrapper = styled.div`
  width: 100%;
  margin-top: 10%;
  display: flex;
  gap: 2rem;
  justify-content: center;
  align-items: center;
  height: 100vh;
  .text-input {
    border: 2px solid red;
    width: 85%;
    height: 32px;
  }
  .button {
    background-color: transparent;
    border: none;
    color: #4a90e2;
    font-size: 10px;
    font-weight: bold;
    width: 100%;
  }
`;
export const FormContainer = styled.div`
  width: 100%;
  max-width: 40%;
  min-height: 50%;
  .resumeWrapper {
    width: 90%;
    height: auto;
    background-color: white;
    position: relative;
    padding: 10px;
  }
  .btn-wraper {
    border: 1px dotted black;
    min-height: fit-content;
    margin: 15%;
    justify-items: center;
    text-align: center;
    padding: 10%;
    display: flex;

    /* text-align: center; */
  }
  .btn {
    height: auto;
    width: auto;
    padding: 5px;
    background-color: #4a90e2;
    color: white;
  }
  .resumeHadlingWrapper {
    width: 90%;
    height: auto;
    background-color: white;
    position: relative;
    padding: 10px;
    margin-top: 15px;
  }
  .keySkiilsWrapper {
    width: 90%;
    height: auto;
    background-color: white;
    position: relative;
    padding: 10px;
    margin-top: 15px;
  }
  .EmploymentWraper {
    width: 90%;
    height: auto;
    background-color: white;
    position: relative;
    padding: 10px;
    margin-top: 15px;
  }
  .EducationWraper {
    width: 90%;
    height: auto;
    background-color: white;
    position: relative;
    padding: 10px;
    margin-top: 15px;
  }
  .ProjectWraper {
    width: 90%;
    height: auto;
    background-color: white;
    position: relative;
    padding: 10px;
    margin-top: 15px;
  }
`;

export const Input = styled.input`
  width: 85%;
  height: 32px;
`;
export const DataWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 30px;
  justify-content: space-between;
`;

export const HeadingWrapper = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  gap: 15px;
`;

export const Button = styled.div`
  border: 2px solid yellow;
  font-size: 10px;
  width: 100%;
`;

// export const ResumeWrapper = styled.div

//   min-height: 5rem;
//   min-width: 100rem;
//   padding-left: 2rem;
//   background-color: white;
// `;

// export const ResumeHeadline = styled.div`
//   min-height: 5rem;
//   min-width: 100rem;
//   padding-left: 2rem;
//   background-color: white;
// `;

// export const KeySlills = styled.div`
//   min-height: 5rem;
//   min-width: 100rem;
//   padding-left: 2rem;
//   background-color: white;
// `;

// export const Employment = styled.div`
//   min-height: 5rem;
//   background-color: white;
//   padding-left: 2rem;
//   min-width: 100rem;

//   .text-input {
//     width: 80%;
//     height: 40px;
//   }
// `;

// export const Education = styled.div`
//   min-height: 5rem;
//   min-width: 100rem;
//   background-color: white;
//   padding-left: 2rem;
//   border-radius: 5px;
// `;

// export const Project = styled.div`
//   min-height: 5rem;
//   min-width: 100rem;
//   padding-left: 2rem;
//   background-color: white;
// `;
