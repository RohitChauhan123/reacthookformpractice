import styled from "styled-components";

export const Container = styled.div`
  height: "100%";
  width: "100%";
  .modal {
    height: "50%";
    width: "50%";
    z-index: 1;
    position: absolute;
    left: 569px;

    top: 100px;
    background-color: pink;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .modal-contect {
    margin-top: 3%;
    width: 500px;
    background-color: "white";
  }

  .modal-header,
  .modal-footer {
    padding: 10px;
  }
  modal-title {
    margin: 0;
  }
  .modal-body {
    padding: 10px;
    border-top: 1px, solid #eee;
    border-bottom: 1px, solid #eee;
  }
`;
