import styled from "styled-components";

export const Container = styled.div`
  .modal {
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    background-color: transparent;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .modal-contect {
    width: 500px;
    background-color: #da9b9b;
  }

  .modal-header,
  .modal-footer {
    padding: 10px;
  }
  modal-title {
    margin: 0;
  }
  .modal-body {
    padding: 10px;
    border-top: 1px, solid #eee;
    border-bottom: 1px, solid #eee;
  }
`;
